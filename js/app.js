(function () {
    var controllerID       = "mainCTRL";
    var sliderControllerID = "slider";
    angular.module("app", [
           "ngRoute",
            "ngResource",
           "ngMap",
           "ui.bootstrap",
           "angularValidator",
           "ngSanitize",
           "ui.router",
           "ngStorage",
           "ngParallax",
           "rzModule",
           "flow",
           "ngSocial",
           "mailchimp",
           "ngCart",
           "hl.sticky",
           "uiRouterStyles",
           "pascalprecht.translate",
           "angular.backtop",
           "angular.filter",
           "720kb.socialshare"
       ])
       .directive('scrollOnClick', function () {
           return {
               restrict: 'A',
               link    : function (scope, $elm, attrs) {
                   var idToScroll = attrs.href;
                   $elm.on('click', function () {
                       var $target;
                       if (idToScroll) {
                           $target = $(idToScroll);
                       }
                       else {
                           $target = $elm;
                       }
                       $("body").animate({scrollTop: $target.offset().top}, "slow");
                   });
               }
           }
       })
       .controller("mainCTRL", ['$scope', 'configService', function ($scope, configService) {
           $scope.filterShow = false;
           $scope.store      = configService;
       }])
       .controller("sliderController", function ($scope, $location, $anchorScroll) {
           $scope.scrollTo = function (id) {
               $location.hash(id);
               $anchorScroll();
           }
       })
       .controller('footer', function ($scope) {
           $scope.footerPara = '/img/paraback.png';
       })
       .controller(controllerID, ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller('topSocial', ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller('tradeIn', ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller('testDrive', ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller('footerConfig', ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller('modalFinance', ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller('testimonial', ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller(sliderControllerID, ['$scope', 'configService', function ($scope, configService) {
           $scope.store = configService;
       }])
       .controller("MapCNTRL", ['$scope', 'NgMap', 'configService', function ($scope, NgMap, configService) {
           $scope.store = configService;
           NgMap.getMap().then(function (map) {
           });
       }])
       .filter("unique", function () {
           return function (collection, keyname) {
               var output = [], keys = [];
               angular.forEach(collection, function (item) {
                   var key = item[keyname];
                   if (keys.indexOf(key) === -1) {
                       keys.push(key);
                       output.push(item);
                   }
               });
               return output;
           };
       });
    $(document).on("click", ".navbar-collapse.in", function (e) {
        if ($(e.target).is("a")) {
            $(this).collapse("hide");
        }
    });
})();