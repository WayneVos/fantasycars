(function () {
    var app = angular.module("app");
    app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider

           .state("home", {
               url  : '/',
               views: {
                   'header@'    : {
                       templateUrl: 'templates/header.html',
                       controller : "PageCtrl"
                   },
                   'bannerHome@': {
                       templateUrl: 'templates/homebanner.html'
                   },
                   ''           : {
                       templateUrl: 'partials/showroom.html'
                   },
                   'footer@'    : {
                       templateUrl: 'templates/footer.html',
                       controller : "PageCtrl"
                   }
               }
           })
           .state("showroom", {
<<<<<<< HEAD
               templateUrl: "partials/showroom.search.list.html",
               url        : "/showroom_search",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
=======
               url  : "/showroom_search",
               views: {
                   'header@': {
                       templateUrl: 'templates/header.html',
                       controller : "PageCtrl"
                   },
                   ''       : {
                       templateUrl: "partials/showroom.search.list.html"
                   },
                   'footer@': {
                       templateUrl: 'templates/footer.html',
                       controller : "PageCtrl"
                   }
>>>>>>> dcdee5c360c7673c6d1ce0cd7fafbdbe051bb45b
               }
           })
           .state("disclaimer", {
               url        : "/disclaimer",
               templateUrl: "partials/disclaimer.html",
<<<<<<< HEAD
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
=======
               controller : "PageCtrl"
>>>>>>> dcdee5c360c7673c6d1ce0cd7fafbdbe051bb45b
           })
           .state("terms", {
               url        : "/terms_conditions",
               templateUrl: "partials/terms_conditions.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("privacy", {
               url        : "/privacy",
               templateUrl: "partials/privacy.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("cardetails", {
               url   : "/:make/:stock_id/:year/:company_id/:dealer/:selling_price/:contact_number/:mileage/:variant/:colour/:condition/:suburb/:address/:location/:province/:body_type/:branch/:extras_csv/:description/:url1/:url2/:url3/:url4/:url5/:url6/:url7/:url8/:url9/:url10",
               params: {
                   car           : null,
                   stock_id      : null,
                   company_id    : null,
                   make          : null,
                   year          : null,
                   selling_price : null,
                   mileage       : null,
                   variant       : null,
                   colour        : null,
                   condition     : null,
                   branch        : null,
                   extras_csv    : null,
                   description   : null,
                   province      : null,
                   contact_number: null,
                   url1          : null,
                   url2          : null,
                   url3          : null,
                   url4          : null,
                   url5          : null,
                   url6          : null,
                   url7          : null,
                   url8          : null,
                   url9          : null,
                   url10         : null
               },
<<<<<<< HEAD
               templateUrl: "partials/cardetails.html",
               controller : ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
                   $scope.car            = $stateParams.car;
                   $scope.stock_id       = $stateParams.stock_id;
                   $scope.make           = $stateParams.make;
                   $scope.year           = $stateParams.year;
                   $scope.variant        = $stateParams.variant;
                   $scope.mileage        = $stateParams.mileage;
                   $scope.colour         = $stateParams.colour;
                   $scope.condition      = $stateParams.condition;
                   $scope.selling_price  = $stateParams.selling_price;
                   $scope.branch         = $stateParams.branch;
                   $scope.extras_csv     = $stateParams.extras_csv;
                   $scope.description    = $stateParams.description;
                   $scope.location       = $stateParams.location;
                   $scope.body_type      = $stateParams.body_type;
                   $scope.company_id     = $stateParams.company_id;
                   $scope.contact_number = $stateParams.contact_number;
                   $scope.address        = $stateParams.address;
                   $scope.dealer         = $stateParams.dealer;
                   $scope.suburb         = $stateParams.suburb;
                   $scope.url1           = $stateParams.url1;
                   $scope.url2           = $stateParams.url2;
                   $scope.url3           = $stateParams.url3;
                   $scope.url4           = $stateParams.url4;
                   $scope.url5           = $stateParams.url5;
                   $scope.url6           = $stateParams.url6;
                   $scope.url7           = $stateParams.url7;
                   $scope.url8           = $stateParams.url8;
                   $scope.url9           = $stateParams.url9;
                   $scope.url10          = $stateParams.url10;
                   $scope.car            = {
                       make          : $scope.make,
                       stock_id      : $scope.stock_id,
                       company_id    : $scope.company_id,
                       dealer        : $scope.dealer,
                       year          : $scope.year,
                       variant       : $scope.variant,
                       mileage       : $scope.mileage,
                       selling_price : $scope.selling_price,
                       colour        : $scope.colour,
                       condition     : $scope.condition,
                       branch        : $scope.branch,
                       extras_csv    : $scope.extras_csv,
                       description   : $scope.description,
                       location      : $scope.location,
                       body_type     : $scope.body_type,
                       contact_number: $scope.contact_number,
                       address       : $scope.address,
                       suburb        : $scope.suburb,
                       url1          : $scope.url1,
                       url2          : $scope.url2,
                       url3          : $scope.url3,
                       url4          : $scope.url4,
                       url5          : $scope.url5,
                       url6          : $scope.url6,
                       url7          : $scope.url7,
                       url8          : $scope.url8,
                       url9          : $scope.url9,
                       url10         : $scope.url10
                   };
=======
               views : {
                   'header@': {
                       templateUrl: 'templates/header.html',
                       controller : "PageCtrl"
                   },
                   ''       : {
                       templateUrl: 'partials/cardetails.html',
                       controller : ["$scope", "$http", "$stateParams", function ($scope, $http, $stateParams) {
                           $scope.car            = $stateParams.car;
                           $scope.stock_id       = $stateParams.stock_id;
                           $scope.make           = $stateParams.make;
                           $scope.year           = $stateParams.year;
                           $scope.variant        = $stateParams.variant;
                           $scope.mileage        = $stateParams.mileage;
                           $scope.colour         = $stateParams.colour;
                           $scope.condition      = $stateParams.condition;
                           $scope.selling_price  = $stateParams.selling_price;
                           $scope.branch         = $stateParams.branch;
                           $scope.extras_csv     = $stateParams.extras_csv;
                           $scope.description    = $stateParams.description;
                           $scope.location       = $stateParams.location;
                           $scope.body_type      = $stateParams.body_type;
                           $scope.company_id     = $stateParams.company_id;
                           $scope.contact_number = $stateParams.contact_number;
                           $scope.address        = $stateParams.address;
                           $scope.dealer         = $stateParams.dealer;
                           $scope.suburb         = $stateParams.suburb;
                           $scope.url1           = $stateParams.url1;
                           $scope.url2           = $stateParams.url2;
                           $scope.url3           = $stateParams.url3;
                           $scope.url4           = $stateParams.url4;
                           $scope.url5           = $stateParams.url5;
                           $scope.url6           = $stateParams.url6;
                           $scope.url7           = $stateParams.url7;
                           $scope.url8           = $stateParams.url8;
                           $scope.url9           = $stateParams.url9;
                           $scope.url10          = $stateParams.url10;
                           $scope.car            = {
                               make          : $scope.make,
                               stock_id      : $scope.stock_id,
                               company_id    : $scope.company_id,
                               dealer        : $scope.dealer,
                               year          : $scope.year,
                               variant       : $scope.variant,
                               mileage       : $scope.mileage,
                               selling_price : $scope.selling_price,
                               colour        : $scope.colour,
                               condition     : $scope.condition,
                               branch        : $scope.branch,
                               extras_csv    : $scope.extras_csv,
                               description   : $scope.description,
                               location      : $scope.location,
                               body_type     : $scope.body_type,
                               contact_number: $scope.contact_number,
                               address       : $scope.address,
                               suburb        : $scope.suburb,
                               url1          : $scope.url1,
                               url2          : $scope.url2,
                               url3          : $scope.url3,
                               url4          : $scope.url4,
                               url5          : $scope.url5,
                               url6          : $scope.url6,
                               url7          : $scope.url7,
                               url8          : $scope.url8,
                               url9          : $scope.url9,
                               url10         : $scope.url10
                           };
                       }]
                   },
                   'footer@': {
                       templateUrl: 'templates/footer.html',
                       controller : "PageCtrl"
                   }
               },
           })
           .state("cardetails.enquire", {
               url           : "/enquire",
               params        : {car: null},
               reloadOnSearch: false,
               onEnter       : ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                   $uibModal.open({
                       templateUrl   : "partials/testdrive.html",
                       resolve       : {
                           car: function () {
                               return $state.params.car;
                           }
                       },
                       backdrop      : "static",
                       keyboard      : false,
                       reloadOnSearch: true,
                       controller    : ["$scope", "$stateParams", function ($scope, $stateParams) {
                           $scope.car           = $state.params.car;
                           $scope.stock_id      = $stateParams.stock_id;
                           $scope.company_id    = $stateParams.company_id;
                           $scope.make          = $stateParams.make;
                           $scope.year          = $stateParams.year;
                           $scope.variant       = $stateParams.variant;
                           $scope.mileage       = $stateParams.mileage;
                           $scope.colour        = $stateParams.colour;
                           $scope.condition     = $stateParams.condition;
                           $scope.selling_price = $stateParams.selling_price;
                           $scope.branch        = $stateParams.branch;
                           $scope.extras_csv    = $stateParams.extras_csv;
                           $scope.description   = $stateParams.description;
                           $scope.location      = $stateParams.location;
                           $scope.url1          = $stateParams.url1;
                           $scope.url2          = $stateParams.url2;
                           $scope.url3          = $stateParams.url3;
                           $scope.url4          = $stateParams.url4;
                           $scope.url5          = $stateParams.url5;
                           $scope.url6          = $stateParams.url6;
                           $scope.url7          = $stateParams.url7;
                           $scope.url8          = $stateParams.url8;
                           $scope.url9          = $stateParams.url9;
                           $scope.url10         = $stateParams.url10;
                           $scope.car           = {
                               make         : $scope.make,
                               year         : $scope.year,
                               variant      : $scope.variant,
                               mileage      : $scope.mileage,
                               selling_price: $scope.selling_price,
                               colour       : $scope.colour,
                               condition    : $scope.condition,
                               branch       : $scope.branch,
                               extras_csv   : $scope.extras_csv,
                               description  : $scope.description,
                               location     : $scope.location,
                               url1         : $scope.url1,
                               url2         : $scope.url2,
                               url3         : $scope.url3,
                               url4         : $scope.url4,
                               url5         : $scope.url5,
                               url6         : $scope.url6,
                               url7         : $scope.url7,
                               url8         : $scope.url8,
                               url9         : $scope.url9,
                               url10        : $scope.url1
                           };
                           $scope.cancel        = function () {
                               $state.transitionTo("showroom");
                           };
                       }]
                   });
>>>>>>> dcdee5c360c7673c6d1ce0cd7fafbdbe051bb45b
               }],
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("cardetails.financecar", {
               url           : "/financecar",
               params        : {car: null},
               reloadOnSearch: false,
               onEnter       : ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                   $uibModal.open({
                       templateUrl   : "partials/modalFinance.html",
                       resolve       : {
                           car: function () {
                               return $state.params.car;
                           }
                       },
                       backdrop      : "static",
                       keyboard      : false,
                       reloadOnSearch: true,
                       controller    : ["$scope", "$stateParams", "$uibModal", function ($scope, $stateParams, $uibModal) {
                           $scope.car           = $state.params.car;
                           $scope.stock_id      = $stateParams.stock_id;
                           $scope.make          = $stateParams.make;
                           $scope.year          = $stateParams.year;
                           $scope.variant       = $stateParams.variant;
                           $scope.mileage       = $stateParams.mileage;
                           $scope.colour        = $stateParams.colour;
                           $scope.condition     = $stateParams.condition;
                           $scope.selling_price = $stateParams.selling_price;
                           $scope.branch        = $stateParams.branch;
                           $scope.extras_csv    = $stateParams.extras_csv;
                           $scope.description   = $stateParams.description;
                           $scope.url1          = $stateParams.url1;
                           $scope.url2          = $stateParams.url2;
                           $scope.url3          = $stateParams.url3;
                           $scope.url4          = $stateParams.url4;
                           $scope.url5          = $stateParams.url5;
                           $scope.url6          = $stateParams.url6;
                           $scope.url7          = $stateParams.url7;
                           $scope.url8          = $stateParams.url8;
                           $scope.url9          = $stateParams.url9;
                           $scope.url10         = $stateParams.url10;
                           $scope.car           = {
                               make         : $scope.make,
                               year         : $scope.year,
                               variant      : $scope.variant,
                               mileage      : $scope.mileage,
                               selling_price: $scope.selling_price,
                               colour       : $scope.colour,
                               condition    : $scope.condition,
                               branch       : $scope.branch,
                               extras_csv   : $scope.extras_csv,
                               description  : $scope.description,
                               url1         : $scope.url1,
                               url2         : $scope.url2,
                               url3         : $scope.url3,
                               url4         : $scope.url4,
                               url5         : $scope.url5,
                               url6         : $scope.url6,
                               url7         : $scope.url7,
                               url8         : $scope.url8,
                               url9         : $scope.url9,
                               url10        : $scope.url1
                           };
                           $scope.cancel        = function () {
                               window.history.back();
                           };
                       }]
                   });
               }],
               controller    : "PageCtrl"
           })
<<<<<<< HEAD
           .state("list", {
               templateUrl: "partials/showroom.search.list.html",
               url        : "/list",
               data       : {title: "Showroom"},
               controller : "PageCtrl"
           })
           .state("list.enquire", {
               url           : "/:make/:stock_id/:year/:selling_price/:mileage/:variant/:colour/:condition/:location/:province/:body_type/:branch/:extras_csv/:description/:url1/:url2/:url3/:url4/:url5/:url6/:url7/:url8/:url9/:url10",
               params        : {car: null},
               reloadOnSearch: false,
               onEnter       : ["$stateParams", "$state", "$uibModal", function ($stateParams, $state, $uibModal) {
                   $uibModal.open({
                       templateUrl   : "partials/testdrive.html",
                       resolve       : {
                           car: function () {
                               return $state.params.car;
                           }
                       },
                       backdrop      : "static",
                       keyboard      : false,
                       reloadOnSearch: true,
                       controller    : ["$scope", "$stateParams", function ($scope, $stateParams) {
                           $scope.car           = $state.params.car;
                           $scope.stock_id      = $stateParams.stock_id;
                           $scope.make          = $stateParams.make;
                           $scope.year          = $stateParams.year;
                           $scope.variant       = $stateParams.variant;
                           $scope.mileage       = $stateParams.mileage;
                           $scope.colour        = $stateParams.colour;
                           $scope.condition     = $stateParams.condition;
                           $scope.selling_price = $stateParams.selling_price;
                           $scope.branch        = $stateParams.branch;
                           $scope.extras_csv    = $stateParams.extras_csv;
                           $scope.description   = $stateParams.description;
                           $scope.location      = $stateParams.location;
                           $scope.url1          = $stateParams.url1;
                           $scope.url2          = $stateParams.url2;
                           $scope.url3          = $stateParams.url3;
                           $scope.url4          = $stateParams.url4;
                           $scope.url5          = $stateParams.url5;
                           $scope.url6          = $stateParams.url6;
                           $scope.url7          = $stateParams.url7;
                           $scope.url8          = $stateParams.url8;
                           $scope.url9          = $stateParams.url9;
                           $scope.url10         = $stateParams.url10;
                           $scope.car           = {
                               make         : $scope.make,
                               year         : $scope.year,
                               variant      : $scope.variant,
                               mileage      : $scope.mileage,
                               selling_price: $scope.selling_price,
                               colour       : $scope.colour,
                               condition    : $scope.condition,
                               branch       : $scope.branch,
                               extras_csv   : $scope.extras_csv,
                               description  : $scope.description,
                               location     : $scope.location,
                               url1         : $scope.url1,
                               url2         : $scope.url2,
                               url3         : $scope.url3,
                               url4         : $scope.url4,
                               url5         : $scope.url5,
                               url6         : $scope.url6,
                               url7         : $scope.url7,
                               url8         : $scope.url8,
                               url9         : $scope.url9,
                               url10        : $scope.url1
                           };
                           $scope.cancel        = function () {
                               $state.transitionTo("list");
                           };
                       }]
                   });
               }],
               controller    : "PageCtrl"
           })
           .state("contact", {
               url        : "/contact",
               templateUrl: "partials/contact.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("spares", {
               url        : "/spares",
               templateUrl: "partials/part.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("cart", {
               url        : "/cart",
               templateUrl: "partials/cart.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("seriesii", {
               url        : "/seriesii",
               templateUrl: "partials/seriesii.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
           })
           .state("checkout", {
               url        : "/checkout",
               templateUrl: "templates/checkout.html",
               controller : "PageCtrl",
               data       : {
                   css: 'css/custom.css'
               }
=======
           .state("about", {
               url       : "/about",
               controller: "PageCtrl",
               views     : {
                   'header@': {
                       templateUrl: 'templates/header.html'
                   },
                   ''       : {
                       templateUrl: 'partials/about.html'
                   },
                   'footer@': {
                       templateUrl: 'templates/footer.html'
                   }
               }
           })
           .state("car_service", {
               url        : "/car_service",
               templateUrl: "partials/car_service.html",
               controller : "PageCtrl"
           })
           .state("car_request", {
               url        : "/car_request",
               templateUrl: "partials/car_request.html",
               controller : "PageCtrl"
           })
           .state("sell_car", {
               url  : "/sell_car",
               views: {
                   'header@': {
                       templateUrl: 'templates/header.html',
                       controller : "PageCtrl"
                   },
                   ''       : {
                       templateUrl: 'partials/sell_car.html',
                       controller : "PageCtrl"
                   },
                   'footer@': {
                       templateUrl: 'templates/footer.html',
                       controller : "PageCtrl"
                   }
               }
           })
           .state("finance", {
               url        : "/finance",
               templateUrl: "partials/finance_blocks.html",
               controller : "PageCtrl"
           })
           .state("contact", {
               url: "/contact",
               views: {
                   'header@': {
                       templateUrl: 'templates/header.html',
                       controller : "PageCtrl"
                   },
                   ''       : {
                       templateUrl: "partials/contact.html",
                   },
                   'footer@': {
                       templateUrl: 'templates/footer.html',
                       controller : "PageCtrl"
                   }
               }
           })
           .state("workshop", {
               url        : "/workshop",
               templateUrl: "partials/workshop.html",
               controller : "PageCtrl"
           })
           .state("testimonial", {
               url        : "/testimonial",
               templateUrl: "partials/testimonial.html",
               controller : "PageCtrl"
>>>>>>> dcdee5c360c7673c6d1ce0cd7fafbdbe051bb45b
           })
           .state("404", {
               url        : "/404",
               templateUrl: "partials/404.html",
               controller : "PageCtrl"
           })
           .state("otherwise", {url: "/404", templateUrl: "partials/404.html", controller: "PageCtrl"});
        $urlRouterProvider.when("", "/home");
        $urlRouterProvider.otherwise("/404");
    });
    app.controller("PageCtrl", function ($scope) {
    });
    /*   app.controller('CartController', ['$scope', '$state', function ($scope, $state) {
     $scope.seriesii = function () {
     $state.go("seriesii");
     }
     }]);*/
    app.controller("dealer", function ($scope) {
        $scope.getSlugifiedName = function (item) {
            return $filter('slugify')(item.name);
        }
    });
    app.run(["$rootScope", "$uibModalStack", function ($rootScope, $uibModalStack) {
        $rootScope.$on("$stateChangeStart", function () {
            var top = $uibModalStack.getTop();
            if (top) {
                $uibModalStack.dismiss(top.key);
            }
        });
    }]);
    app.config(["$stateProvider", "$urlRouterProvider", function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider.state("/", {url: "/home", templateUrl: "partials/showroom.html"});
    }]);
})();