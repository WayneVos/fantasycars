(function () {
    angular.module("app")
       .controller('CartController', function ($scope, $location, $anchorScroll, $http, $state, ngCart) {
              var vm                    = this;
              vm.data                   = [];
              vm.dataFacets             = [];
              vm.modelData              = [];
              vm.partData               = [];
              vm.pGroupData             = [];
              vm.searchData             = [];
              $scope.pageLimit          = 6;
              $scope.page               = 9;
              $scope.ngCart             = ngCart;
              var companyIdFilter       = "?company_id=eq.445";
               var dataUrlBasePath       = "http://lrservicecentre.vmgtest.co.za/api/v1/all_parts?";
              //var dataUrlBasePath       = "http://lrservicecentre.vmgtest.co.za/api/v1/all_parts_with_facets?";
              var dataFacetsUrlBasePath = "http://lrservicecentre.vmgtest.co.za/api/v1/all_facets?";
// Numeric Sorting
              $scope.setItemsPerPage    = function (num) {
                  $scope.itemsPerPage = num;
              };
              $scope.perPage            = {item: ["9", "15", "21", "33"]};
              $scope.showAll            = false;
              $scope.$watch("[showAll]", function () {
                  $scope.itemsPerPage = !$scope.showAll;
                  if ($scope.showAll === false) {
                      $scope.itemsPerPage = $scope.page;
                  }
                  else {
                      $scope.itemsPerPage = vm.data.length;
                      $scope.carQuantity  = $scope.page;
                  }
              }, true);
//Filter start------------------------------>
              $scope.currentPage = 1;
              ShowController($http, $state);
              ShowController.$inject = ["$http", $state];
              function ShowController($http, $state) {
                  refreshFilters();
                  resetVehicles();
              }

              function fetchFilteredData() {
                  var currentOffset = ($scope.currentPage - 1);
                  var filterUrl     = (getModelSelectText() !== "" ? "&model=eq." + getModelSelectText() : "")
                     + (getpGroupSelectText() !== "" ? "&part_group=eq." + getpGroupSelectText() : "")
                     + (getpartSelectText() !== "" ? "&part_sub_group=eq." + getpartSelectText() : "");
                   $http.get(dataUrlBasePath, {cache: true}).then(function (response) {
                   $scope.totalProducts = response.data.length;
                   });
                   $http.get(dataUrlBasePath + "&offset=" + currentOffset + filterUrl).then(function (response) {
                   vm.data = response.data;
                   });

                  event.preventDefault(); // scroll on filter
                  angular.element("html,body").animate({scrollTop: "0px"}, "slow");
              }

              function getModelSelectText() {
                  return $scope.modelSelect ? $scope.modelSelect.model : "";
              }

              function getpGroupSelectText() {
                  return $scope.pGroupSelect ? $scope.pGroupSelect.part_group : "";
              }

              function getpartSelectText() {
                  return $scope.partSelect ? $scope.partSelect.part_sub_group : "";
              }

              function resetVehicles() {
                  $scope.modelSelect  = "";
                  $scope.pGroupSelect = "";
                  $scope.partSelect   = "";
                  $scope.showAll      = false;
                  $http.get(dataUrlBasePath).then(function (response) {
                      $scope.totalProducts = response.data.length;
                  });
                  var currentOffset = ($scope.currentPage - 1) * vm.limit;
                  $http.get(dataUrlBasePath + "&offset=" + currentOffset).then(function (response) {
                      vm.data = response.data;
                  });
              }

              function refreshFilters() {
                  $http.get(dataUrlBasePath + "select=model,part_group,part_sub_group").then(function (response) {
                      vm.modelData  = [];
                      vm.partData   = [];
                      vm.pGroupData = [];
                      for (var counter = 0; counter < response.data.length; counter++) {
                          vm.modelData.push(response.data[counter]);
                          vm.partData.push(response.data[counter]);
                          vm.pGroupData.push(response.data[counter]);
                      }
                  });
              }

              $scope.modelChangedSelect  = function () {
                  var model = getModelSelectText();
                  if (model !== "") {
                      $http.get(dataUrlBasePath + "select=part_group,part_sub_group"
                         + (model !== "" ? "&model=eq." + model : "")).then(function (response) {
                          vm.pGroupData = [];
                          vm.partData   = [];
                          for (var counter = 0; counter < response.data.length; counter++) {
                              vm.pGroupData.push(response.data[counter]);
                              vm.partData.push(response.data[counter]);
                          }
                      });
                  }
                  else {
                      refreshFilters();
                  }
                  $scope.currentPage = 1;
                  fetchFilteredData();
              };
              $scope.pGroupChangedSelect = function () {
                  var part_group = getpGroupSelectText();
                  var model      = getModelSelectText();
                  if (part_group !== "") {
                      $http.get(dataUrlBasePath + "select=part_sub_group"
                         + (model !== "" ? "&model=eq." + model : "")
                         + (part_group !== "" ? "&part_group=eq." + part_group : "")).then(function (response) {
                          vm.partData = [];
                          for (var counter = 0; counter < response.data.length; counter++) {
                              vm.partData.push(response.data[counter]);
                          }
                      });
                  }
                  else {
                      refreshFilters();
                  }
                  $scope.currentPage = 1;
                  fetchFilteredData();
              };
              $scope.partChangedSelect   = function () {
                  if (getpartSelectText() !== "") {
                  }
                  else {
                  }
                  $scope.currentPage = 1;
                  fetchFilteredData();
              };
              $scope.filter              = function () {
                  fetchFilteredData();
              };
              $scope.clearFilter         = function () {
                  refreshFilters();
                  resetVehicles();
              };
              $scope.pageChanged         = function () {
                  fetchFilteredData();
              };
          }
       )
    //Filter END------------------------------>
})();