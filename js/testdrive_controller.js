(function () {
    var controllerId = "testDrive";
    angular.module("app").controller(controllerId, ['$scope', '$state', 'configService', testDrive]);
    function testDrive($scope, $state, configService) {
        $scope.store = configService;
        var vm       = this;
        vm.drive     = {
            name         : "",
            number       : "",
            email        : "",
            message      : "",
            make         : "",
            variant      : "",
            year         : "",
            selling_price: "",
            company_id   : "",
            stock_id     : ""
        };
        function spinner() {
            document.getElementById("submitbut2").addEventListener("click", function () {
                angular.element("#spinner").show();
            });
        }

        vm.financeClick = function (car) {
            vm.detailsdata    = car;
            $state.params.car = car;
            $state.go("cardetails.financecar", {car: car});
        };
        spinner();
        vm.sendMail = function sendEmail() {
            var driveRequest = "";
            var data         = {
                notification: {
                    ToList        : $scope.store.eMail.carEnquireEmail,
                    CcList        : $scope.store.eMail.ccEmail,
                    BccList       : $scope.store.eMail.bccEmail,
                    Subject       : "Enquiry from " + vm.drive.name + ", with regards to: " + vm.drive.make,
                    Message       : vm.drive.message
                    + "\nContact Number: " + vm.drive.number
                    + "\nStock ID: " + vm.drive.stock_id
                    + "\nCompany ID: " + vm.drive.company_id
                    + "\nMake: " + vm.drive.make
                    + "\nVariant: " + vm.drive.variant
                    + "\nYear: " + vm.drive.year
                    + "\nPrice: R" + vm.drive.selling_price,
                    MessageSubject: "Enquiry from " + vm.drive.name,
                    ToName        : $scope.store.eMail.carEnquireEmailTo,
                    FromName      : vm.drive.name,
                    FromEmail     : vm.drive.email
                }
            };
            driveRequest     = JSON.stringify(data);
            $.ajax({
                url     : "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method  : "POST",
                headers : {"content-type": "application/json", accept: "application/json"},
                data    : driveRequest,
                dataType: "json",
                success : function (response) {
                    document.getElementById("testdriveForm").reset();
                    angular.element("#testdriveForm").hide();
                    angular.element("#mailsent").show();
                    angular.element("#spinner").hide();
                },
                error   : function (response) {
                    document.getElementById("testdriveForm").reset();
                    angular.element("#testdriveForm").hide();
                    angular.element("#mailnotsent").show();
                }
            });
        };
    }
})();