(function () {
    var controllerId = "contactUsMon";
    angular.module("app").controller(controllerId, ['$scope', 'configService', contactUsMon]);

    function contactUsMon($scope, configService) {
        $scope.store = configService;
        var vm = this;
        vm.details = {name: "", number: "", email: "", message: ""};
        function spinnerMon() {
            document.getElementById("submitMon").addEventListener("click", function () {
                angular.element("#spinnerMon").css("display", "block");
            });
        }

        spinnerMon();

        vm.sendMail = function sendEmail() {
            var contactRequestMon = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.contactEmail,
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: "Contact request from " + vm.details.name + ", Montaque Gardens",
                    Message: vm.details.message + "\n\nContact me on: " + vm.details.number,
                    MessageSubject: "Contact request from " + vm.details.name,
                    ToName: $scope.store.eMail.contactEmailTo,
                    FromName: vm.details.name,
                    FromEmail: vm.details.email
                }
            };
            contactRequestMon = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {"content-type": "application/json", accept: "application/json"},
                data: contactRequestMon,
                dataType: "json",
                success: function (response) {
                    document.getElementById("contactMontague").reset();
                    angular.element("#contactMontague").css("display", "none");
                    angular.element("#mailsentMon").css("display", "block");
                    angular.element("#spinnerMon").css("display", "none");

                },
                error: function (response) {
                    document.getElementById("contactMontague").reset();
                    angular.element("#contactMontague").css("display", "none");
                    angular.element("#mailnotsentMon").css("display", "block");
                }
            });
        };
    }
})();