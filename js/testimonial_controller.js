(function () {
    var controllerId = "testimonial";
    angular.module("app").controller(controllerId, ['$scope', 'configService', testiMonial]);
    var vm  = this;
    vm.data = [];
    function testiMonial($scope, configService, $http) {
        $scope.store = configService;
        var vm       = this;
        vm.testim = {
            name   : "",
            number : "",
            email  : "",
            subject: "",
            comment: ""
        };
        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var testimonialsend = "";
            var data            = {
                notification: {
                    ToList        : $scope.store.eMail.testimonialEmail,
                    CcList        : "",
                    BccList       : "webmaster@vmgsoftware.co.za",
                    Subject       : "Testimonial from " + vm.testim.name,
                    MessageSubject: vm.testim.subject,
                    Message       : vm.testim.comment
                    + "\n\nContact me on: " + vm.testim.number
                    + "\nPosted on: " + new Date(),
                    ToName        : vm.testim.dealer,
                    FromName      : vm.testim.name,
                    FromEmail     : vm.testim.email
                }
            };
            testimonialsend     = JSON.stringify(data);
            $.ajax({
                url     : "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method  : "POST",
                headers : {"content-type": "application/json", accept: "application/json"},
                data    : testimonialsend,
                dataType: "json",
                success : function (response) {
                    document.getElementById("testiMonials").reset();
                    angular.element("#testiMonials").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error   : function (response) {
                    document.getElementById("testiMonials").reset();
                    angular.element("#testiMonials").css("display", "none");
                    angular.element("#mailnotsent").css("display", "block");
                }
            });
        };
    }
})();