(function () {
    var controllerId = "carService";
    angular.module("app").controller(controllerId, ['$scope', 'configService', carService]);
    function carService($scope, configService) {
        $scope.store = configService;
        var vm       = this;
        vm.service   = {
            name     : "",
            email    : "",
            phone    : "",
            dealer   : "",
            make     : "",
            model    : "",
            year     : "",
            mileage  : "",
            vin      : "",
            date     : "",
            type     : "",
            firsttime: "",
            message  : ""
        };
        function spinner() {
            document.getElementById("buttsubmit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var serviceRequest = "";
            var data           = {
                notification: {
                    ToList        : $scope.store.eMail.carServiceEmail,
                    CcList        : "",
                    BccList       : "webmaster@vmgsoftware.co.za",
                    Subject       : "Car Service Request",
                    Message       : vm.service.message
                    + "\n\nContact Number: " + vm.service.phone
                    + "\nDealership: " + vm.service.dealer
                    + "\nMake: " + vm.service.make
                    + "\nModel: " + vm.service.model
                    + "\nYear: " + vm.service.year
                    + "\nMileage: " + vm.service.mileage
                    + "\nVin Number: " + vm.service.vin
                    + "\nDate: " + vm.service.date
                    + "\nType: " + vm.service.type
                    + "\nHave we serviced you before: " + vm.service.firsttime,
                    MessageSubject: "Car Service Request: " + vm.service.make + ", " + vm.service.model,
                    ToName        : $scope.store.eMail.carServiceEmailTo,
                    FromName      : vm.service.name,
                    FromEmail     : vm.service.email
                }
            };
            serviceRequest     = JSON.stringify(data);
            $.ajax({
                url     : "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method  : "POST",
                headers : {"content-type": "application/json", accept: "application/json"},
                data    : serviceRequest,
                dataType: "json",
                success : function (response) {
                    document.getElementById("carService").reset();
                    angular.element("#carService").css("display", "none");
                    angular.element("#mailsent").css("display", "block");
                    angular.element("#spinner").css("display", "none");
                },
                error   : function (response) {
                    document.getElementById("carService").reset();
                    document.getElementById("carService").style.display  = "none";
                    document.getElementById("mailnotsent").style.display = "block";
                }
            });
        };
    }
})();