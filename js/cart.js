(function () {
    "use strict";
    angular.module("app")
       .controller('cart', ['$scope', '$http', 'ngCart', function ($scope, $http, ngCart) {
           ngCart.setTaxRate(7.5);
           ngCart.setShipping(2.99);
       }
       ])
})();