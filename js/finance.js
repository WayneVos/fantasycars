(function () {
    var controllerId = "Finance";
    angular.module("app").controller(controllerId, Finance);
    function Finance($scope) {
        var vm = this;
        vm.fin = {name: "", lastname: "", email: "", number: "", salary: "", year: "", license: ""};
        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                $("#spinner").show();
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var finance   = "";
            var companyId = "46";
            var data      = {
                notification: {
                    ToList: "sales@a1auto1.co.za",
                    CcList: "",
                    BccList: "webmaster@vmgsoftware.co.za",
                    Subject: vm.fin.name + " needs finance.",
                    Message: "\nSalary over R6500: " + vm.fin.salary + "\nModel between 2006 and 2016: " + vm.fin.year + "\nDo you have a license: " + vm.fin.license + "\nContact Number: " + vm.fin.number,
                    MessageSubject: "I need finance.",
                    ToName: "A1 Auto1",
                    FromName: vm.fin.name + " " + vm.fin.lastname,
                    FromEmail: vm.fin.email
                }
            };
            finance       = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {"content-type": "application/json", accept: "application/json"},
                data: finance,
                dataType: "json",
                success: function (response) {
                    //window.location = "https://www.seritisolutions.co.za/zahfa/web/finance/financeapplication.aspx?CompanyCode=VMGHFA&UniqueCode=4ACF2FD6-A64D-4148-9A90-80FCB3648605";
                    document.getElementById("Finance").reset();
                    angular.element("#Finance").hide();
                    angular.element("#mailsent").show();
                    angular.element("#spinner").hide();
                },
                error: function (response) {
                    document.getElementById("Finance").reset();
                    angular.element("#Finance").hide();
                    angular.element("#mailnotsent").show();
                }
            });
        };
    }
})();