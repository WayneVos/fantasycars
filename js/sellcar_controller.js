(function () {
    'use strict';

    var controllerId = 'tradeIn';
    angular.module('app').controller(controllerId,['$scope', 'configService',  tradeIn]);

    function tradeIn($scope,configService) {
        $scope.store = configService;
        var vm = this;
        vm.details = {
            make: '',
            model: '',
            year: '',
            mileage: '',
            name: '',
            lastName: '',
            phoneNumber: '',
            email: '',
            comments: ''
        };

        spinner();
        function spinner() {
            document.getElementById("submit").addEventListener("click", function () {
                angular.element("#spinner").css("display", "block");
            });
        }

        vm.sendEmail = sendEmail;
        vm.totalFileSize = totalFileSize;


        function buildSimpleMessageWithAttachments(details, files) {
            return {
                ToList: $scope.store.eMail.carSellEmail,
                CcList: "",
                BccList: $scope.store.eMail.ccEmail,
                Subject: "I would like to sell my vehicle.",
                Message: buildTradeInMessageString(details),
                MessageSubject: 'I would lke to sell my vehicle',
                ToName: $scope.store.eMail.carSellEmailTo,
                FromName: details.name + ' ' + details.lastName,
                FromEmail: details.email,
                Attachments: files
            };
        }

        function sendEmail() {

            $.each(vm.flow.files, function (index, file) {
                var progress = file.progress();
                if (progress != 1) {
                    alert("Please wait while loading image " + file.name);
                    
                }
            });

            var imageObjs = $('#image-files img');
            var imageContents = {};

            imageObjs.each(function (index, image) {
                imageContents[image.alt] = image.src;
            });
            var notification = buildSimpleMessageWithAttachments(vm.details, imageContents);

            var data = "{'notification':" + JSON.stringify(notification) + "}";
            var maxJsonLength = 100485760; // 10 MiB
            if (data.length > maxJsonLength) {
                alert("Your images are too big (more than 10 MiB). Please reduce the number and/or size of the images");
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmailWithAttachments",
                data: data,
                dataType: "json",
                success: function (response) {
                    angular.element("#spinner").css("display", "none");
                    var delayredirect = 500;
                    setTimeout(function () {
                        window.location = '/#/showroom';
                    }, delayredirect);

                },
                error: function (response) {
                }
            });
        }

        function totalFileSize(flow) {
            var files = flow.files;
            var sum = 0;
            $.each(files, function (index, file) {
                sum += file.size;
            });
            return sum;
        }

        function buildTradeInMessageString(details) {
            var newLine = '\n';
            return 'Name: ' + details.name + ' ' + details.lastName + newLine
                + 'Phone Number: ' + details.phoneNumber + newLine
                + 'Email: ' + details.email + newLine + newLine
                + 'Make: ' + details.make + newLine
                + 'Model: ' + details.model + newLine
                + 'Year: ' + details.year + newLine
                + 'Mileage: ' + details.mileage + newLine
                + 'Comments: ' + details.comments;
        }

    }
})();