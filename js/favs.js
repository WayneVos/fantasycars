(function () {
    "use strict";
    angular.module('app')
        .service('LocalStorageService', [
            '$window', function ($window) {
                var service = {
                    store: store,
                    retrieve: retrieve,
                    clear: clear,
                    clearAll: clearAll
                };

                return service;

                function store(key, value) {
                    $window.localStorage.setItem(key, angular.toJson(value, false));
                }

                function retrieve(key) {
                    return $window.localStorage.getItem(key);
                    // return angular.fromJson($window.localStorage.getItem(key));
                    // I'm not 100% sure, but I think I need to de-serialize the json that was stored
                }

                function clear(key) {
                    $window.localStorage.removeItem(key);
                }

                function clearAll() {
                    $window.localStorage.clear();
                }

            }
        ]);
})();