(function () {
    var controllerId = "modalFinance";
    angular.module("app").controller(controllerId,['$scope', 'configService',  finApp]);
    function finApp($scope,configService) {
        $scope.store = configService;
        var vm = this;
        vm.modalFin = {
            name: "",
            lastname: "",
            idnumber: "",
            number: "",
            email: "",
            message: "",
            permission: "",
            deposit: "",
            depositprice: "",
            tradein: "",
            tradeindetails: "",
            companyid: "",
            carid: "",
            make: "",
            variant: "",
            year: "",
            selling_price: ""

        };
        function spinner() {
            document.getElementById("submitbut2").addEventListener("click", function () {
                angular.element("#spinner").show();
            });
        }

        spinner();
        vm.sendMail = function sendEmail() {
            var financeApp = "";
            var data = {
                notification: {
                    ToList: $scope.store.eMail.carFinanceEmail,
                    CcList: vm.modalFin.email,
                    BccList: $scope.store.eMail.ccEmail,
                    Subject: "Finance Application from " + vm.modalFin.name + ", with regards to: " + vm.modalFin.make,
                    Message: vm.modalFin.message
                    + "\nIdentity/Passport Number: " + vm.modalFin.idnumber
                    + "\nContact Number: " + vm.modalFin.number
                    + "\nMake: " + vm.modalFin.make
                    + "\nModel: " + vm.modalFin.variant
                    + "\nYear: " + vm.modalFin.year
                    + "\nPrice: R" + vm.modalFin.selling_price
                    + "\nNCR Credit Check: " + vm.modalFin.permission
                    + "\nDeposit: " + vm.modalFin.deposit
                    + "\nDeposit Amount: R" + vm.modalFin.depositprice
                    + "\nTrade In: " + vm.modalFin.tradein
                    + "\nTrade In Details: " + vm.modalFin.tradeindetails,
                    MessageSubject: "Finance Application from " + vm.modalFin.name,
                    ToName: $scope.store.eMail.carFinanceEmailTo,
                    FromName: vm.modalFin.name + ' ' + vm.modalFin.lastname,
                    FromEmail: vm.modalFin.email
                }
            };
            financeApp = JSON.stringify(data);
            $.ajax({
                url: "http://www.vmgsoftware.co.za/ShowroomMailServiceV3/MailService.asmx/SendSimpleEmail",
                method: "POST",
                headers: {"content-type": "application/json", accept: "application/json"},
                data: financeApp,
                dataType: "json",
                success: function (response) {
                    document.getElementById("finAppForm").reset();
                    angular.element("#finAppForm").hide();
                    angular.element("#mailsent").show();
                    angular.element("#spinner").hide();
                },
                error: function (response) {
                    document.getElementById("finAppForm").reset();
                    angular.element("#finAppForm").hide();
                    angular.element("#mailnotsent").show();
                }
            });
        };
    }
})();